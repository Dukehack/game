// background.h: interface for the background class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BACKGROUND_H__5F4BFAA8_A3F9_4806_9CF0_6874B2378084__INCLUDED_)
#define AFX_BACKGROUND_H__5F4BFAA8_A3F9_4806_9CF0_6874B2378084__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "allegro.h"
extern BITMAP* buffer;

class background  
{
public:
	background();
	virtual ~background();
	void draw();
private:
	int l[2];
};

#endif // !defined(AFX_BACKGROUND_H__5F4BFAA8_A3F9_4806_9CF0_6874B2378084__INCLUDED_)
