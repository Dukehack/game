// bullet.cpp: implementation of the bullet class.
//
//////////////////////////////////////////////////////////////////////

#include "bullet.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bullet::bullet()
{
}

bullet::~bullet()
{
}

void bullet::draw()
{
	// you have blue bullets, invaders have yellow bullets
	if(vy < 0)
		circlefill(buffer, x, y, 2, makecol(0, 100, 180));
	else
		circlefill(buffer, x, y, 2, makecol(255, 255, 0));

	// move each time it's drawn
	y+=vy;
	if(y<0 || y>798) active = false;
}





