#include <fstream>
#include "allegro.h"
#include "player.h"
#include "invader.h"
#include "bullet.h"
#include "background.h"
#include "destroyed.h"

// sprites / sounds
BITMAP* buffer;
BITMAP* invaderbmp;
BITMAP* shipbmp;
SAMPLE* fire;
SAMPLE* explode;

// objects
player player1;
invader bug[10][5];
bullet bul[100];
background back;
destroyed destroy;

int cycle;
int level;
int hiscore;
bool running = true;

int dv;

void draw(void);

void intro()
{
	bool cont = false;

	do
	{
	} while(key[KEY_ENTER]);

	do
	{
		draw();

		textout(buffer, font, "Duke Invaders", 300, 300, makecol(200, 0, 0));
		textout(buffer, font, "Escape to quit...", 300, 320, makecol(200, 0, 0));
		textout(buffer, font, "Enter to play...", 300, 340, makecol(200, 0, 0));

		blit(buffer, screen, 0, 0, 0, 0, 800, 600);

		if(key[KEY_ENTER])
			cont = true;
		if(key[KEY_ESC])
		{
			cont = true;
			running = false;
		}
	} while(!cont && running);
}

void init(bool f)
{
	// f - true		reset game
	// f - false	reset level

	// default no enemys
	for(int x = 0;x<10;x++)
		for(int y = 0;y<5;y++)
			bug[x][y].kill();

	// clear active bullets
	for(int x = 0;x<100;x++)
	{
		bul[x].destroy();
	}

	if(f)
	{
		player1.init();
		level = 1;
		intro();
	}

	dv = 2;

	// more enemys depending on level
	int rows = level;
	if(rows>5) rows = 5;

	for(int x = 0;x<10;x++)
		for(int y = 0;y<rows;y++)
			bug[x][y].setpos(x*48, y*32);
}

void levelcomp()
{
	player1.addscore(level*1000);
	level++;

	char printstr[50];
	do
	{
		draw();
		sprintf(printstr, "Level %d complete!", level-1);
		textout(buffer, font, printstr, 200, 300, makecol(200, 0, 0));
		textout(buffer, font, "Press enter to continue...", 200, 320, makecol(200, 0, 0));
		blit(buffer, screen, 0, 0, 0, 0, 800, 600);
	} while(!key[KEY_ENTER] && running && !key[KEY_ESC]);
}

void gameover()
{
	if(player1.getscore() >= hiscore)
	{
		hiscore = player1.getscore();

		std::ofstream scores("hiscore.txt", std::ofstream::out);
		scores << hiscore;
		scores.close();
	}

	char printstr[50];
	do
	{
	} while(key[KEY_ESC]);
	do
	{
		draw();
		sprintf(printstr, "Game Over, with a score of %d", player1.getscore());
		textout(buffer, font, printstr, 200, 300, makecol(200, 0, 0));
		textout(buffer, font, "Press enter to continue...", 200, 320, makecol(200, 0, 0));
		blit(buffer, screen, 0, 0, 0, 0, 800, 600);
	} while(!key[KEY_ENTER] && running && !key[KEY_ESC]);
}

void idle(void)
{
	// update player
	// 1 - lose life
	if(player1.move() == 1)
	{
		if(player1.getlives() > 0)
			init(false);
		else
		{
			// game over
			gameover();
			init(true);
		}
	}

	// cancel game
	if(key[KEY_ESC])
	{
		gameover();
		init(true);
	}

	// update hiscore
	if(player1.getscore() > hiscore)
		hiscore = player1.getscore();

	// update invaders
	bool col = 0;
	bool bugsleft = false;

	for(int x = 0;x<10;x++)
	{
		for(int y = 0;y<5;y++)
		{
			if(bug[x][y].isactive())
			{
				bugsleft = true;
				if(bug[x][y].move(dv)) {col = true;}
			}
		}
	}

	// change direction / move down
	if(col)
	{
		dv = 0-dv;
		cycle++;
		if(cycle > 1)
		{
			cycle = 0;
			for(int x = 0;x<10;x++)
				for(int y = 0;y<5;y++)
					bug[x][y].movedown();
		}
	}

	if(!bugsleft)
	{
		levelcomp();
		init(false);
	}
}

void draw(void)
{
	back.draw();

	// draw effects for destroyed invaders
	destroy.draw();

	// draw active bullets
	for(int x = 0;x<100;x++)
		if(bul[x].isactive()) bul[x].draw();

	// draw invaders
	for(int x = 0;x<10;x++)
		for(int y = 0;y<5;y++)
			if(bug[x][y].isactive()) bug[x][y].draw();

	// draw player's ship
	player1.draw();

	// draw HUD
	for(int x = 0;x<player1.getlives();x++)
		draw_sprite(buffer, shipbmp, x*40, 550);

	char printstr[50];
	sprintf(printstr, "Hiscore : %d", hiscore);
	textout(buffer, font, printstr, 600, 530, makecol(200, 0, 0));
	sprintf(printstr, "Score : %d", player1.getscore());
	textout(buffer, font, printstr, 600, 550, makecol(200, 0, 0));
	sprintf(printstr, "Level : %d", level);
	textout(buffer, font, printstr, 600, 570, makecol(200, 0, 0));
}

int load_resources()
{


	std::ofstream error("errors.txt", std::ofstream::out);
	// graphics
	set_color_depth(16);
	if(set_gfx_mode(GFX_AUTODETECT, 800, 600, 0, 0)!=0)
	{
		error << "ERROR set_gfx_mode" ; error.close(); return 1;
	}
	set_pallete(desktop_pallete);
	clear_to_color(screen, 255);
	text_mode(-1);

	buffer = create_bitmap(800, 600);

	// load bitmaps

	//int load_resources();
	PALETTE pal;
	invaderbmp = load_bitmap("invader.bmp", pal);
		if(!invaderbmp) {error << "ERROR load invader.bmp" ; error.close(); return 1; }
	shipbmp = load_bitmap("ship.bmp", pal);
		if(!shipbmp) {error << "ERROR load ship.bmp" ; error.close(); return 1; }
	set_palette(pal);

	// load sounds
	fire = load_sample("fire.wav");
		if(!fire) {error << "ERROR load fire.wav" ; error.close(); return 1; }
	explode = load_sample("explode.wav");
		if(!explode) {error << "ERROR load explode.wav"; error.close(); return 1; }

	// no errors, close file and continue
	error.close();
}
