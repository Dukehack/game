// player.h: interface for the player class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLAYER_H__074AA784_85D0_465F_8BC7_8E8C816EBB4B__INCLUDED_)
#define AFX_PLAYER_H__074AA784_85D0_465F_8BC7_8E8C816EBB4B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "allegro.h"
#include "bullet.h"

extern BITMAP *buffer;
extern BITMAP *shipbmp;
extern SAMPLE *fire;
extern SAMPLE *explode;
extern bullet bul[100];

class player  
{
public:
	player();
	virtual ~player();

	void init()
	{
		x = 384;
		firedelay = 0;
		score = 0;
		lives = 3;
		active = 1;
	}

	void draw();
	int move();

	void loselife()
	{
		active = -1;
		lives--;
		play_sample(explode, 255, 128, 1000, false);
	}

	void addscore(int s) { score += s; }
	int getscore() { return score; }
	int getlives() { return lives; }
private:
	int x;
	int firedelay;

	int active; // time since destroyed
				// negative inactive
				// positive active

	int score;
	int lives;
};

#endif // !defined(AFX_PLAYER_H__074AA784_85D0_465F_8BC7_8E8C816EBB4B__INCLUDED_)
